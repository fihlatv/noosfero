# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2015-05-07 11:11-0300\n"
"Last-Translator: daniel tygel <dtygel@eita.org.br>\n"
"Language-Team: pt_BR <dtygel@eita.org.br>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

msgid "Consumer interests updated"
msgstr "Interesses de consumo atualizados"

msgid "Sniffs product suppliers and consumers near to your enterprise."
msgstr ""
"Busca fornecedores e consumidores de produtos próximos ao seu empreendimento."

msgid "Consumer Interests"
msgstr "Interesses de consumo"

msgid "Opportunities Sniffer"
msgstr "Farejador de oportunidades"

msgid "Lists declared and inputs interests"
msgstr "Mostra insumos e interesses de consumo"

msgid "Lists interests"
msgstr "Mostra interesses"

msgid "Interests"
msgstr "Interesses"

msgid "This block show interests of your profile or environment"
msgstr "Este bloco mostra seus interesses ou da rede"

msgid "Edit %{inputs} and %{block.interests}"
msgstr "Editar %{inputs} e %{block.interests}"

msgid "products' inputs"
msgstr "Insumos de produtos"

msgid "declared interests"
msgstr "interesses declarados"

msgid "%{interest} from %{profile}"
msgstr "%{interest} de %{profile}"

msgid "Suppliers"
msgstr "Fornecedores"

msgid "Consumers"
msgstr "Consumidores"

msgid "Both"
msgstr "Ambos"

msgid "look for products..."
msgstr "buscar por produtos..."

msgid "Maximum distance:"
msgstr "Distância máxima:"

msgid "Buyer interests"
msgstr "Interesses de compra"

msgid ""
"Select here products and services categories that you have an interest on "
"buying. Then you can go to the Opportunity Sniffer and check out enterprises "
"near you that offer such products. Type in some characters and choose your "
"interests from our list."
msgstr ""
"Selecione aqui as categorias de produtos e serviços que você tem interesse "
"em comprar. Com isso, empreendimentos saberão suas necessidades e poderão te "
"fazer ofertas.Digite e escolha quais são seus interesses."

msgid "Type in a keyword"
msgstr "Escreva uma palavra chave"

msgid "Save"
msgstr "Salvar"

msgid "Back to control panel"
msgstr "Voltar ao painel de controle"

msgid "Can supply"
msgstr "Fornece"

msgid "May consum"
msgstr "Consome"

msgid "Close"
msgstr "Fechar"

msgid "Up to %s km from you. %s result(s) found."
msgstr "Até %s km de você. %s resultado(s) encontrados."

msgid "Legend"
msgstr "Legenda"

msgid "your enterprise"
msgstr "seu empreendimento"

msgid "suppliers"
msgstr "fornecedores"

msgid "consumers"
msgstr "consumidores"

msgid "both"
msgstr "ambos"
